# Set the default LCG_XX release version
lcg_xx=88
lcgrel=

#-------------------------------------------------------------------------------
# Check that this setup script has been sourced
#-------------------------------------------------------------------------------

if [ "$BASH_SOURCE" = "$0" ]; then # See CORALCOOL-2823
  echo "ERROR! The setup script was not sourced?" > /dev/stderr
  exit 1
elif [ "$BASH_SOURCE" = "" ]; then
  echo "ERROR! The setup script was not sourced from bash?" > /dev/stderr
  return 1
fi

# Determine the path to this sourced script
topdir=`dirname ${BASH_SOURCE}`
topdir=`cd ${topdir}; pwd`

#-------------------------------------------------------------------------------
# Parse command line arguments
#-------------------------------------------------------------------------------

usage()
{ 
  echo "Usage:   . $BASH_SOURCE [-b <BINARY_TAG>] [-l <xx_for_lcgxx>|-r <path_to_lcgrel>]" > /dev/stderr
  echo "Example: . $BASH_SOURCE -l $lcg_xx"
  echo "Example: . $BASH_SOURCE -r /cvmfs/sft.cern.ch/lcg/releases/LCG_$lcg_xx"
  echo "Example: . $BASH_SOURCE -b x86_64-slc6-clang37-opt -r /afs/cern.ch/sw/lcg/app/nightlies/dev2/Mon"
  echo "Example: . $BASH_SOURCE -b aarch64-ubuntu1404-gcc49-opt -r /home/${USER}/nightlies/20160207"
  echo "Default: . $BASH_SOURCE -l $lcg_xx [using \$BINARY_TAG=$BINARY_TAG]"
  echo ""
  echo "Use $BASH_SOURCE to set up CORAL/COOL builds from an LCG release/nightly area"
  echo "- set BINARY_TAG from the -b option or the \$BINARY_TAG env variable"
  echo "- prepend paths to \$CMAKE_PREFIX_PATH from the -l or -r options"
  echo "- set up the compiler (set CC and CXX) from the resulting \$BINARY_TAG"
  echo "- set CMAKE_BUILD_TYPE from the resulting \$BINARY_TAG"
  echo "- set CMAKE_CXX_FLAGS from the resulting \$BINARY_TAG"
  echo "- set up ccache and ninja if the executables are found"
}

# Optionally choose an LCG release from command line arguments
# [Default: use the environment variable BINARY_TAG previously set by the user]
if [ "$1" == "-b" ]; then
  if [ "$2" != "" ]; then export BINARY_TAG=$2; shift 2; else usage; return 1; fi
fi

# Optionally choose an LCG release from command line arguments
# [Default: use the variable lcg_xx defined at the top of this script]
if [ "$1" == "-r" ]; then
  if [ "$2" != "" ]; then lcgrel=${2%/}; shift 2; else usage; return 1; fi
else
  if [ "$1" == "-l" ]; then
    if [ "$2" != "" ]; then lcg_xx=$2; shift 2; else usage; return 1; fi
  fi
  if [ -d /cvmfs ]; then
    lcgrel=/cvmfs/sft.cern.ch/lcg/releases/LCG_${lcg_xx}
  else
    lcgrel=/afs/cern.ch/sw/lcg/releases/LCG_${lcg_xx}
  fi
fi

# Fail if unexpected arguments are present
if [ "$*" != "" ]; then
  usage
  return 1
fi

# Sanity check on command line arguments
echo "INFO: set up CORAL/COOL against ${lcgrel}"
if [ ! -d "$lcgrel" ]; then
  echo "ERROR! Directory '${lcgrel}' does not exist" > /dev/stderr
  return 1
fi

#-------------------------------------------------------------------------------
# Check that BINARY_TAG has been set
#-------------------------------------------------------------------------------

# Require BINARY_TAG to be set a priori (CORALCOOL-2846).
# [Do not set BINARY_TAG from host arch/os/compiler any more (CORALCOOL-2851)]
# [Do not set BINARY_TAG from CMTCONFIG/CMAKECONFIG any more (CORALCOOL-2850)]
if [ "$BINARY_TAG" == "" ]; then
  echo "ERROR! BINARY_TAG is not set" > /dev/stderr
  exit 1
fi
echo "INFO: set up CORAL/COOL for BINARY_TAG ${BINARY_TAG}"

#-------------------------------------------------------------------------------
# Set up CMAKE_PREFIX_PATH for the given release (CORALCOOL-2846)
#-------------------------------------------------------------------------------

# Set up CMAKE_PREFIX_PATH for the given release (CORALCOOL-2846)
# Keep both tcmalloc and gperftools for now (eventually drop tcmalloc)
# [NB CORAL is included, any local build must be prepended a posteriori]
for pkgname in ccache Boost CppUnit expat frontier_client mysql oracle Python QMtest sqlite XercesC gperftools tcmalloc igprof libaio libunwind valgrind CORAL ROOT tbb qt; do 
  pkgpath=${lcgrel}/${pkgname}
  if [ -d ${pkgpath} ]; then 
    export CMAKE_PREFIX_PATH=`find ${pkgpath} -mindepth 2 -maxdepth 2 -name ${BINARY_TAG}`:${CMAKE_PREFIX_PATH}
  fi 
done

#-------------------------------------------------------------------------------
# Set up the compiler using a $BINARY_TAG-specific script (CORALCOOL-2957),
# in principle the same script used by lcgjenkins to configure lcgcmake.
# This is identified by a CORAL/COOL script, as no central mechanism exists.
# [Previously the compiler was set up using the LHCb wrappers corresponding
# to the compiler in the "LCG summary" file LCG_externals_${BINARY_TAG}.txt]
#-------------------------------------------------------------------------------

if [ -f $topdir/compilerSetup.sh ]; then
  . $topdir/compilerSetup.sh $BINARY_TAG
else
  echo "WARNING! $topdir/compilerSetup.sh not found!?" > /dev/stderr
fi

# Warn if CC and CXX have not been set by now
# [Note that lcgcmake relies on lcgjenkins/jk-setup.sh]
if [ "$CC" == "" ]; then echo "WARNING! CC is not set" > /dev/stderr; fi
if [ "$CXX" == "" ]; then echo "WARNING! CXX is not set" > /dev/stderr; fi

#-------------------------------------------------------------------------------
# Set up CMAKE_BUILD_TYPE from LCG build type in BINARY_TAG (CORALCOOL-2846)
#-------------------------------------------------------------------------------

# Set CMAKE_BUILD_TYPE from the LCG build type in BINARY_TAG
# [See lcg_get_target_platform in heptools-common.cmake from lcgcmake]
lcgbty=${BINARY_TAG##*-}
if [ "$lcgbty" == "opt" ]; then
  CMAKE_BUILD_TYPE=Release
elif [ "$lcgbty" == "dbg" ]; then
  CMAKE_BUILD_TYPE=Debug
elif [ "$lcgbty" == "cov" ]; then
  CMAKE_BUILD_TYPE=Coverage
elif [ "$lcgbty" == "pro" ]; then
  CMAKE_BUILD_TYPE=Profile
elif [ "$lcgbty" == "o2g" ]; then
  CMAKE_BUILD_TYPE=RelWithDebInfo
elif [ "$lcgbty" == "min" ]; then
  CMAKE_BUILD_TYPE=MinSizeRel
else
  echo "ERROR! Unknown LCG build type: '$lcgbty'" > /dev/stderr
  return 1
fi

#-------------------------------------------------------------------------------
# Set up CMAKE_CXX_FLAGS from BINARY_TAG (CORALCOOL-2854 and CORALCOOL-2846)
#-------------------------------------------------------------------------------

# Build flags (C++ std)
if [[ $BINARY_TAG == *clang* ]]; then
  CMAKE_CXX_FLAGS="-std=c++14"
elif [[ $BINARY_TAG == *gcc6* ]]; then # CORALCOOL-2895 and CORALCOOL-2913
  CMAKE_CXX_FLAGS="-std=c++14"
elif [[ $BINARY_TAG == *gcc5* ]]; then
  CMAKE_CXX_FLAGS="-std=c++14"
elif [[ $BINARY_TAG == *gcc49* ]]; then
  CMAKE_CXX_FLAGS="-std=c++1y"
elif [[ $BINARY_TAG == *gcc48* ]]; then
  CMAKE_CXX_FLAGS="-std=c++11"
fi

# Build flags (gcc-toolchain options for clang)
# [NB These are now no longer passed to lcgcmake (CORALCOOL-2846): is this ok?]
if [[ $BINARY_TAG == *mac* ]]; then
  : # noop
elif [[ $BINARY_TAG == *clang35* ]]; then # See CORALCOOL-2821
  CMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} --gcc-toolchain=/cvmfs/sft.cern.ch/lcg/releases/gcc/4.9.1/x86_64-slc6" # cvmfs instead of AFS, see CORALCOOL-2855
elif [[ $BINARY_TAG == *clang37* ]]; then # See CORALCOOL-2821
  CMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} --gcc-toolchain=/cvmfs/sft.cern.ch/lcg/releases/gcc/4.9.3/x86_64-slc6" # cvmfs instead of AFS, see CORALCOOL-2855
fi

#-------------------------------------------------------------------------------
# Set up Boost extra configuration for CMake as in lcgcmake (SPI-842)
#-------------------------------------------------------------------------------

# Follow the same logic as in lcgcmake/cmake/toolchain/heptools-common.cmake 
# See CORALCOOL-2806 (mac), SPI-842 (clang) and CORALCOOL-2797 (icc)
if [[ $BINARY_TAG == *mac* ]]; then
  Boost_COMPILER=" -DBoost_COMPILER=-xgcc42"
elif [[ $BINARY_TAG == *clang* ]]; then
  Boost_COMPILER=${BINARY_TAG%-*}
  Boost_COMPILER=" -DBoost_COMPILER=-${Boost_COMPILER#*-*-}"
elif [[ $BINARY_TAG == *icc* ]]; then
  Boost_COMPILER=" -DBoost_COMPILER=-il"
else
  Boost_COMPILER=
fi

# Always add Boost 1.59 (LCG80-84) and 1.61 (LCG85 and above)
# See CORALCOOL-2806 (1.59) and CORALCOOL-2903 (1.61) 
# Add only the two-digit Boost version as in Pere's lcgcmake patch (SPI-842)
Boost_extra_configuration="-DBoost_NO_BOOST_CMAKE=ON -DBoost_ADDITIONAL_VERSIONS='1.59 1.61'$Boost_COMPILER"

#-------------------------------------------------------------------------------
# Set up CMake
#-------------------------------------------------------------------------------

# Use CMake 3.6.0 from /cvmfs/sft.cern.ch/lcg/contrib on Linux
# Use AFS if no cvmfs (e.g. on buildcoverity, SPI-863)
# [Do not use the LCG stack cmake on Linux as it may use a different GLIBCXX]
if [ `uname -s` != "Linux" ]; then
  if [ -d ${lcgrel}/CMake ]; then
    export PATH=`find ${lcgrel}/CMake -mindepth 2 -maxdepth 2 -name ${BINARY_TAG}`/bin:${PATH}
  fi
elif [ -d /cvmfs ]; then
  export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.6.0/Linux-x86_64/bin:$PATH
else
  export PATH=/afs/cern.ch/sw/lcg/contrib/CMake/3.6.0/Linux-x86_64/bin:$PATH
fi

#-------------------------------------------------------------------------------
# Set up ninja
#-------------------------------------------------------------------------------

# Use ninja 1.4.0 from /cvmfs/sft.cern.ch/lcg/contrib/ninja on Linux
# Use AFS if no cvmfs (e.g. on buildcoverity, SPI-863)
# The hardcoded ninja path is valid on Linux but not on Darwin (CORALCOOL-2911)
# [Do not use the LCG stack ninja on Linux as it may use a different GLIBCXX]
if [ `uname -s` != "Linux" ]; then
  if [ -d ${lcgrel}/ninja ]; then
    export PATH=`find ${lcgrel}/ninja -mindepth 2 -maxdepth 2 -name ${BINARY_TAG}`/bin:${PATH}
    export CMAKE_PREFIX_PATH=`find ${lcgrel}/ninja -mindepth 2 -maxdepth 2 -name ${BINARY_TAG}`:${CMAKE_PREFIX_PATH}
  fi
else
  if [ -d /cvmfs/sft.cern.ch/lcg/contrib/ninja/1.7.2.gcc0ea.kitware.dyndep-1/x86_64-slc6 ]; then
    export PATH=/cvmfs/sft.cern.ch/lcg/contrib/ninja/1.7.2.gcc0ea.kitware.dyndep-1/x86_64-slc6:$PATH
  else
    export PATH=/afs/cern.ch/sw/lcg/contrib/ninja/1.4.0/x86_64-slc6:$PATH
  fi
fi

#-------------------------------------------------------------------------------
# Set up ccache
#-------------------------------------------------------------------------------

# Use the installation in the LCG release if it exists (CORALCOOL-2846)
if [ -d ${lcgrel}/ccache ]; then
  export PATH=`find ${lcgrel}/ccache -mindepth 2 -maxdepth 2 -name ${BINARY_TAG}`/bin:${PATH}
fi

# Configure the ccache directory (CORALCOOL-2844 and CORALCOOL-2846)
# Keep this here to allow interactive queries in this shell
export CCACHE_DIR=$topdir/.ccache
export CCACHE_CONFIGPATH=$CCACHE_DIR/ccache.conf

#-------------------------------------------------------------------------------
# Set up command line options to CMake
#-------------------------------------------------------------------------------

CMAKEFLAGS=

# Use ninja by default, if it exists, in the build rules generated by CMake
# [This is a CMake standard variable]
if ninja --version > /dev/null 2>&1; then 
  CMAKEFLAGS="-GNinja $CMAKEFLAGS"
fi

# Set BINARY_TAG (CORALCOOL-2849)
# [This is a CORAL/COOL variable, not a CMake standard variable]
CMAKEFLAGS="-DBINARY_TAG=$BINARY_TAG $CMAKEFLAGS"

# Set CMAKE_BUILD_TYPE
# [This is a CMake standard variable]
CMAKEFLAGS="-DCMAKE_BUILD_TYPE=$CMAKE_BUILD_TYPE $CMAKEFLAGS"

# Set CMAKE_CXX_FLAGS
CMAKEFLAGS="-DCMAKE_CXX_FLAGS=$CMAKE_CXX_FLAGS $CMAKEFLAGS"

# Set CMAKE_VERBOSE_MAKEFILE
# ** This is now enabled by default only in lcgcmake and cc-build, not here! **
# [This is a CMake standard variable]
# This sets VERBOSE=1 for make and passes -v as ninja command line argument
# Note that CMake >= 3.3 is required fo this to have an effect on ninja
# See https://github.com/ninja-build/ninja/issues/900
###CMAKEFLAGS="-DCMAKE_VERBOSE_MAKEFILE=ON $CMAKEFLAGS"

# Set Boost_ADDITIONAL_VERSIONS, Boost_COMPILER, Boost_NO_BOOST_CMAKE
# [These are CMake standard variables]
CMAKEFLAGS="$Boost_extra_configuration $CMAKEFLAGS"

# Enable ccache by default
# [This is a CORAL/COOL variable, not a CMake standard variable]
CMAKEFLAGS="-DCMAKE_USE_CCACHE=ON $CMAKEFLAGS"

# Relocate paths below LCG_releases_base (CORALCOOL-2829)
# [This is a CORAL/COOL variable, not a CMake standard variable]
CMAKEFLAGS="-DLCG_releases_base:STRING=${lcgrel} $CMAKEFLAGS"

# Set up Python3 (CORALCOOL-2976)
# [This is a CORAL/COOL variable, not a CMake standard variable]
if [ -d ${lcgrel}/Python ]; then
  if [ -f `find ${lcgrel}/Python -mindepth 2 -maxdepth 2 -name ${BINARY_TAG}`/bin/python3 ]; then 
    CMAKEFLAGS="-DLCG_python3=on $CMAKEFLAGS"
    # Also set up Python_ADDITIONAL_VERSIONS
    Python_config_version_twodigit=$(`find ${lcgrel}/Python -mindepth 2 -maxdepth 2 -name ${BINARY_TAG}`/bin/python3 --version | awk '{split($2,a,"."); print a[1]"."a[2]}')
    CMAKEFLAGS="-DPython_ADDITIONAL_VERSIONS=$Python_config_version_twodigit $CMAKEFLAGS"
    CMAKEFLAGS="-DPython_config_version_twodigit=$Python_config_version_twodigit $CMAKEFLAGS"
  fi
fi

# Export CMAKEFLAGS for future use in the CORAL/COOL Makefile
# [** NB: This is a CORAL/COOL variable, not a CMake standard variable! **]
export CMAKEFLAGS

#-------------------------------------------------------------------------------
# Set up optional configuration parameters
#-------------------------------------------------------------------------------

# Optionally override installation directory location
# Use absolute paths or paths relative to the _build_ directory!
###export CMAKEFLAGS="-DCMAKE_INSTALL_PREFIX=../my.${BINARY_TAG} $CMAKEFLAGS"
###export CMAKEFLAGS="-DCMAKE_INSTALL_PREFIX=/tmp/my.${BINARY_TAG} $CMAKEFLAGS"

# Add setup overrides if any are present
if [ -f $topdir/overrideSetupCMake.sh ]; then
  echo "INFO: override defaults using $topdir/overrideSetupCMake.sh"
  . $topdir/overrideSetupCMake.sh
else
  echo "INFO: no overrides found"
fi

#-------------------------------------------------------------------------------
# Dump all variables set by the setupLCG.sh script
#-------------------------------------------------------------------------------

###echo BINARY_TAG=$BINARY_TAG
###echo CMAKE_PREFIX_PATH=$CMAKE_PREFIX_PATH
###echo CMAKEFLAGS=$CMAKEFLAGS
###echo PATH=$PATH
###echo CC=$CC
###echo CXX=$CXX
###echo LCG_release_area=$LCG_release_area
###echo LCG_hostos=$LCG_hostos
###echo CCACHE_DIR=$CCACHE_DIR
###echo CCACHE_CONFIGPATH=$CCACHE_CONFIGPATH
