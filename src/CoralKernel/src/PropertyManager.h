#ifndef PROPERTYMANAGER_H
#define PROPERTYMANAGER_H 1

// Include files
#include <map>
#include <memory>
#include "CoralBase/../src/coral_mutex_headers.h"
#include "CoralKernel/IPropertyManager.h"

namespace coral
{

  /**
   * Class PropertyManager
   * Implementation of the IPropertyManager class.
   * PropertyManager objects own a mutex to provide thread safety.
   * The object stores a fixed property set, the set cannot be modified
   * in runtime (the property values can be modified).
   *///
  class PropertyManager
    : public IPropertyManager
  {
  public:

    PropertyManager();
    virtual ~PropertyManager();
    virtual IProperty* property(const std::string&);

  private:

    void setDefaultProperties();
    void addProperty( const char* key, const char* value );

  private:

    std::map<std::string, std::shared_ptr<IProperty> > m_cont;
    coral::mutex m_mutex;

  };

}

#endif // PROPERTYMANAGER_H
