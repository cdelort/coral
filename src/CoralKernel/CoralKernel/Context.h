#ifndef CORALKERNEL_CONTEXT_H
#define CORALKERNEL_CONTEXT_H 1

// Include files
#include <map>
#include <memory> // task #49014
#include <mutex> // task #49014
#include <set>
#include <string>
#include "CoralKernel/IHandle.h"
#include "CoralKernel/ILoadableComponent.h"

namespace coral
{

  class IPluginManager;
  class IPropertyManager;
  class PluginManager;

  /// The context singleton class.
  class Context
  {
  public:

    /// Returns the instance of the singleton
    static Context& instance(); // See bug #73566

    /**
       Loads a component given its name and an optional pointer to an external
       plugin manager (this option is used by CMS, see CORALCOOL-1585).
       Throws an exception in case of a problem
    *///
    void loadComponent( const std::string& componentName,
                        coral::IPluginManager* pluginManager = 0 );

    /// Returns a handle for a specified interface
    template< typename T > IHandle<T> query()
    {
      std::lock_guard<std::mutex> lock( m_mutex );
      for ( std::map<std::string, ILoadableComponent*>::iterator
              iComponent = m_components.begin(); iComponent != m_components.end(); ++iComponent )
      {
        T* component = dynamic_cast<T*>( iComponent->second );
        if ( component != 0 )
        {
          iComponent->second->addReference();
          return IHandle<T>( component );
        }
      }
      // Return an empty handle
      return IHandle<T>();
    }

    /// Returns a handle for a specified interface given a name
    template< typename T > IHandle<T> query( const std::string& name )
    {
      std::lock_guard<std::mutex> lock( m_mutex );
      std::map<std::string, ILoadableComponent*>::iterator iComponent = m_components.find( name );
      if ( iComponent == m_components.end() ) return IHandle<T>();
      T* component = dynamic_cast<T*>( iComponent->second );
      if ( component != 0 )
      {
        iComponent->second->addReference();
        return IHandle<T>( component );
      }
      else
      {
        return IHandle<T>();
      }
    }

    /// Returns the CORAL property manager component
    /// This was requested by CMS to avoid environment variables (task #6857)
    IPropertyManager& propertyManager(); // task #30840

    /// Returns the list of plugins known by the internal CORAL plugin manager
    std::set<std::string> knownComponents() const;

    /// Returns the list of plugins loaded into the Context,
    /// by the internal or an external plugin manager
    std::set<std::string> loadedComponents() const;

  private:

    /// Constructor
    Context(); // See bug #73566

    /// Copy constructor is private
    Context( const Context& );

    /// Assignment operator is private
    Context& operator=( const Context& );

    /// Destructor
    ~Context();

    /// Checks if a plugin has been loaded into the Context,
    /// by the internal or an external plugin manager
    bool existsComponent( const std::string& componentName );

  private:

    /// The mutex lock for the component map
    std::mutex m_mutex;

    /// The type of the loaded plugin map
    typedef std::map<std::string, ILoadableComponent*> m_components_type;

    /// The map of plugins loaded by the internal or an external plugin manager
    m_components_type m_components;

    /// The internal plugin manager
    std::unique_ptr<coral::PluginManager> m_pluginManager;

    /// The property manager
    std::unique_ptr<coral::IPropertyManager> m_propertyManager;
  };

}
#endif
