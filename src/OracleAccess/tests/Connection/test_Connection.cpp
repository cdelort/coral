// Include files
#include <cstdlib>
#include <memory>
#include "CoralBase/TimeStamp.h"
#include "CoralBase/../tests/Common/CoralCppUnitDBTest.h"
#include "CoralCommon/Utilities.h"
#include "CoralKernel/Context.h"
#include "RelationalAccess/IAuthenticationCredentials.h"
#include "RelationalAccess/IAuthenticationService.h"
#include "RelationalAccess/IDatabaseServiceDescription.h"
#include "RelationalAccess/IDatabaseServiceSet.h"
#include "RelationalAccess/ILookupService.h"
#include "RelationalAccess/AccessMode.h"
#include "RelationalAccess/ConnectionService.h"
#include "RelationalAccess/ConnectionServiceException.h"
#include "CoralCommon/IConnection.h"
#include "RelationalAccess/IConnectionServiceConfiguration.h"
#include "RelationalAccess/IRelationalDomain.h"
#include "RelationalAccess/ISchema.h"
#include "CoralCommon/ISession.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/ITransaction.h"
#include "RelationalAccess/RelationalServiceException.h"

// The connection strings for CORAL tests
const std::string coralConnStringAdmin = coral::CoralCppUnitDBTest::BuildUrl( "Oracle", false );

namespace coral
{

  class ConnectionTest : public CoralCppUnitTest
  {

    CPPUNIT_TEST_SUITE( ConnectionTest );
    CPPUNIT_TEST( test_SimpleConnection );
    //CPPUNIT_TEST( test_expiredPassword_bug77440 ); // CORALCOOL-998
    CPPUNIT_TEST_SUITE_END();

  public:

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    void
    test_SimpleConnection()
    {
      // Fix bug #99879 - START
      std::string connectionString = coralConnStringAdmin;
      std::auto_ptr<coral::IDatabaseServiceSet> dbSet; // fix bug #99879
      coral::Context& context = coral::Context::instance();
      context.loadComponent( "CORAL/Services/XMLLookupService" );
      coral::IHandle<coral::ILookupService> lookupSvc = context.query<coral::ILookupService>( "CORAL/Services/XMLLookupService" );
      if ( ! lookupSvc.isValid() )
        throw std::runtime_error( "Can't load Lookup Service" );
      context.loadComponent( "CORAL/Services/XMLAuthenticationService" );
      coral::IHandle<coral::IAuthenticationService> authSvc =
        context.query<coral::IAuthenticationService>(  "CORAL/Services/XMLAuthenticationService" );
      if ( ! authSvc.isValid() )
        throw std::runtime_error( "Can't load Authentication Service" );
      dbSet.reset( lookupSvc->lookup( connectionString ) );
      if( dbSet->numberOfReplicas() == 0 )
        throw std::runtime_error( "No replicas found" );
      const coral::IDatabaseServiceDescription& svdesc = dbSet->replica( 0 );
      const coral::IAuthenticationCredentials& creds = authSvc->credentials( svdesc.connectionString() );
      // Fix bug #99879 - END

      // Fix bug #99881 - START
      context.loadComponent( "CORAL/RelationalPlugins/oracle" );
      coral::IHandle<coral::IRelationalDomain> iHandle = context.query<coral::IRelationalDomain>( "CORAL/RelationalPlugins/oracle" );
      if ( ! iHandle.isValid() )
      {
        throw coral::NonExistingDomainException( "oracle" );
      }
      std::pair<std::string, std::string> connectionAndSchema = iHandle->decodeUserConnectionString( svdesc.connectionString() );
      coral::IConnection* connection = iHandle->newConnection( connectionAndSchema.first );
      if ( ! connection->isConnected() )
        connection->connect();
      coral::ISession* session = connection->newSession( connectionAndSchema.second );
      session->startUserSession( creds.valueForItem( creds.userItem() ), creds.valueForItem( creds.passwordItem() ) );
      // Fix bug #99881 - END

      session->transaction().start();

      const std::set<std::string>& tables = session->nominalSchema().listTables();

      std::cout << "Retrieved " << tables.size() << " tables:" << std::endl;
      unsigned int i = 0;
      for ( std::set<std::string>::const_iterator it=tables.begin(); it != tables.end(); ++it, ++i )
      {
        if ( i<10 || i>tables.size()-10 )
          std::cout << "  " << *it << std::endl;
        else if ( i==10 )
          std::cout << "  ..." << std::endl;
      }

      session->transaction().commit();

      session->transaction().start( true );

      session->endUserSession();
      if ( session->isUserSessionActive() )
      {
        throw std::runtime_error( "Session appears to be up and running !!!" );
      }

      session->startUserSession( creds.valueForItem( creds.userItem() ), creds.valueForItem( creds.passwordItem() ) );

      if ( ! session->isUserSessionActive() )
      {
        throw std::runtime_error( "Connection lost..." );
      }

      session->transaction().start();

      delete session;
      delete connection; // fix bug #99881
    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    /*
    void
    test_expiredPassword_bug77440() // aka CORALCOOL-998 (about ORA-28001)
    {
      const std::string coralConnStringExpired = coralConnStringAdmin.substr( 0, coralConnStringAdmin.size() - 5 ) + std::string("expired");
      // NB This test was originally developed to test failed connection
      // with expired passwords, but does not explicitly expect ORA-28001;
      // as we no longer have such a test account, we just test it with an
      // invalid username/password (which will give ORA-01017 instead).
      coral::ConnectionService connSvc;
      coral::IConnectionServiceConfiguration& config = connSvc.configuration();
      int timeout = 20; // Reduce timeout from 300 to 20 seconds (bug #78276 aka CORALCOOL-1007)
      config.setConnectionRetrialPeriod( 4 );
      config.setConnectionRetrialTimeOut( timeout );
      long long start = coral::TimeStamp::now().total_nanoseconds();
      try
      {
        std::auto_ptr<coral::ISessionProxy> session( connSvc.connect( coralConnStringExpired ) );
      }
      catch( coral::ConnectionNotAvailableException& e )
      {
        std::cout << "ConnectionNotAvailableException caught: " << e.what() << std::endl;
        long long end = coral::TimeStamp::now().total_nanoseconds();
        if ( (end-start)/1000000000LL < timeout )
        {
          std::cout << "Exception caught within timeout: OK" << std::endl;
        }
        else
        {
          std::cout << "ERROR! Exception caught after timeout exceeded" << std::endl;
          throw;
        }
      }
    }
    */

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    void setUp()
    {
      coral::ConnectionService connSvc;
      coral::IConnectionServiceConfiguration& config = connSvc.configuration();
      static int defPeriod = config.connectionRetrialPeriod();
      static int defTimeOut = config.connectionRetrialTimeOut();
      config.setConnectionRetrialPeriod( defPeriod );
      config.setConnectionRetrialTimeOut( defTimeOut );
    }

    void tearDown()
    {
    }

  };

  CPPUNIT_TEST_SUITE_REGISTRATION( ConnectionTest );

}

CORALCPPUNITTEST_MAIN( ConnectionTest )
