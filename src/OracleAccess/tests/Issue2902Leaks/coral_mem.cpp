#include <iostream>
#include <string>
#include <cstdlib>
#include <unistd.h>
#include <sys/resource.h>

#include <RelationalAccess/ConnectionService.h>
#include <RelationalAccess/IConnectionServiceConfiguration.h>
#include <RelationalAccess/ISessionProxy.h>

using namespace std;

namespace
{
  coral::ConnectionService conn_serv;
  // print RSS high water mark and diff to previous value
  struct rusage print_rss(const char* mark, const int iloop, struct rusage* old=nullptr)
  {
    struct rusage usage;
    getrusage(RUSAGE_SELF, &usage);
    cout << mark << " loop#" << iloop << " - current maxrss: " << usage.ru_maxrss;
    if (old)
    {
      cout << " diff: " << (usage.ru_maxrss - old->ru_maxrss);
    }
    cout << endl;
    return usage;
  }
}

int main(int argc, char** argv)
{
  if (argc < 2)
  {
    cerr << "Usage: " << argv[0] << " conn_str [repeat_count]" << endl;
    return 1;
  }
  int repeat = 100;
  std::string connStr = argv[1];
  if (argc > 2) repeat = atoi(argv[2]);
  bool update = false;
  //conn_serv.setMessageVerbosityLevel(coral::Debug);
#if 0
  // uncomment line below to close connection after each session,
  // this fixes memory issues
  coral::IConnectionServiceConfiguration& config = conn_serv.configuration();
  config.setConnectionTimeOut(0);
#endif
  for (; repeat; -- repeat)
  {
    auto usage1 = print_rss("before new session", repeat);
    auto accessMode = update ? coral::Update : coral::ReadOnly;
    coral::ISessionProxy* session = ::conn_serv.connect(connStr, accessMode);
    auto usage2 = print_rss("after new session", repeat, &usage1);
    delete session;
    auto usage3 = print_rss("after delete session", repeat, &usage2);
    sleep(1);
    cout << "\n";
  }
}
