#include "Cursor.h"
#include "SessionProperties.h"
#include "Statement.h"
#include "RelationalAccess/SchemaException.h"

coral::FrontierAccess::Cursor::Cursor( Statement* statement,
                                       const coral::AttributeList& rowBuffer ) :
  m_statement( statement ),
  m_rowBuffer( rowBuffer ),
  m_started( false )
{
}


coral::FrontierAccess::Cursor::~Cursor()
{
  this->close();
}


bool
coral::FrontierAccess::Cursor::next()
{
  // Fix bug #75094 aka CORALCOOL-952
  // (cursor::next should fail if transaction is not active)
  if( !m_statement->sessionProperties().isTransactionActive() )
    throw coral::QueryException( m_statement->sessionProperties().domainServiceName(), "Transaction is not active", "fetchNext()" );
  if ( m_statement == 0 ) return false;
  bool result = m_statement->fetchNext();
  if ( result && !m_started ) m_started=true;  // fix bug #91028
  if ( ! result ) {
    this->close();
  }
  return result;
}


const coral::AttributeList&
coral::FrontierAccess::Cursor::currentRow() const
{
  if ( !m_started ) // fix bug #91028
    throw coral::QueryException( "Frontier",
                                 "Cursor loop has not started yet",
                                 "ICursor::currentRow()" );
  return m_rowBuffer;
}


void
coral::FrontierAccess::Cursor::close()
{
  if ( m_statement != 0 ) {
    delete m_statement;
    m_statement = 0;
  }
}
