#ifndef CORALCOMMON_IMONITORINGCONTROLLER_H
#define CORALCOMMON_IMONITORINGCONTROLLER_H 1

// IMonitoringController was moved to CoralCommon in CORAL3 (CORALCOOL-1589)
// This is no longer protected by an #ifdef (CORALCOOL-2941)

#include <iosfwd>
#include "RelationalAccess/MonitoringLevel.h"

namespace coral
{

  /**
   * Class IMonitoringController
   *
   * User-level interface for controlling the client-side monitoring
   * for the current session.
   *///
  class IMonitoringController
  {

  public:

    /**
     * Starts the client-side monitoring for the current session.
     * Throws a MonitoringServiceNotFoundException
     * if there is no monitoring service available.
     *///
    virtual void start( monitor::Level level = monitor::Default ) = 0;

    /**
     * Stops the client side monitoring.
     * Throws a monitoring exception if something went wrong.
     *///
    virtual void stop() = 0;

    /**
     * Reports whatever was gathered by the monitoring service to an ostream.
     * Throws a MonitoringServiceNotFoundException
     * if there is no monitoring service available.
     *///
    virtual void reportToOutputStream( std::ostream& os ) const = 0;

    /**
     * Triggers the reporting of the underlying monitoring service.
     * Throws a MonitoringServiceNotFoundException
     * if there is no monitoring service available.
     *///
    virtual void report() const = 0;

    /**
     * Returns the status of the monitoring.
     *///
    virtual bool isActive() const = 0;

  protected:

    /// Protected empty destructor
    virtual ~IMonitoringController() {}

  };

}

#endif // CORALCOMMON_IMONITORINGCONTROLLER_H
