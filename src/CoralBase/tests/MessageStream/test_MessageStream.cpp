#include <iostream>
#include "CoralBase/MessageStream.h"

int main( int, char** )
{
  for ( int lvl=0; lvl<9; lvl++ )
  {
    std::cout << "*** VERBOSITY=" << lvl << "***" << std::endl;
    coral::MessageStream::setMsgVerbosity( (coral::MsgLevel)lvl );
    // Create a message stream
    coral::MessageStream msg01("Test01");
    msg01 << coral::NumLevels << "Hello world NUMLEVELS" << coral::MessageStream::endmsg; // SAME AS ALWAYS
    msg01 << coral::Always << "Hello world ALWAYS" << coral::MessageStream::endmsg;
    msg01 << coral::Fatal << "Hello world FATAL" << coral::MessageStream::endmsg;
    msg01 << coral::Error << "Hello world ERROR" << coral::MessageStream::endmsg;
    msg01 << coral::Warning << "Hello world WARNING" << coral::MessageStream::endmsg;
    msg01 << coral::Info << "Hello world INFO" << coral::MessageStream::endmsg;
    msg01 << coral::Debug << "Hello world DEBUG" << coral::MessageStream::endmsg;
    msg01 << coral::Verbose << "Hello world VERBOSE" << coral::MessageStream::endmsg;
    msg01 << coral::Nil << "Hello world NIL" << coral::MessageStream::endmsg; // NEVER PRINTED OUT!
    std::cout << std::endl;
    // Create a message stream a la CORAL_SERVER
    coral::MessageStream("Test02") << coral::NumLevels << "Hello world NUMLEVELS" << coral::MessageStream::endmsg; // SAME AS ALWAYS
    coral::MessageStream("Test02") << coral::Always << "Hello world ALWAYS" << coral::MessageStream::endmsg;
    coral::MessageStream("Test02") << coral::Fatal << "Hello world FATAL" << coral::MessageStream::endmsg;
    coral::MessageStream("Test02") << coral::Error << "Hello world ERROR" << coral::MessageStream::endmsg;
    coral::MessageStream("Test02") << coral::Warning << "Hello world WARNING" << coral::MessageStream::endmsg;
    coral::MessageStream("Test02") << coral::Info << "Hello world INFO" << coral::MessageStream::endmsg;
    coral::MessageStream("Test02") << coral::Debug << "Hello world DEBUG" << coral::MessageStream::endmsg;
    coral::MessageStream("Test02") << coral::Verbose << "Hello world VERBOSE" << coral::MessageStream::endmsg;
    coral::MessageStream("Test02") << coral::Nil << "Hello world NIL" << coral::MessageStream::endmsg; // NEVER PRINTED OUT!
    std::cout << std::endl;
  }
}
