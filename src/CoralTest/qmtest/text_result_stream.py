import os
from qm.test.base import *
from qm.test.result import *
from qm.test.classes.text_result_stream import TextResultStream as qmTextResultStream
class TextResultStream(qmTextResultStream):
    """This is a modified version of the default TextResultStream class."""


    def WriteResult(self, result):
        """Output a test or resource result.

        'result' -- A 'Result'."""

        # Record the results as they are received.
        if result.GetKind() == Result.TEST:
            # Remember how many tests we have processed.
            self.__num_tests += 1
            # Increment the outcome count.
            outcome = result.GetOutcome()
            self.__outcome_counts[outcome] += 1
            # Remember tests with unexpected results so that we can
            # display them at the end of the run.
            test_id = result.GetId()
            expected_outcome = self._GetExpectedOutcome(result.GetId())
            if self.format != "stats" and outcome != expected_outcome:
                self.__unexpected_outcome_counts[outcome] += 1
                self.__unexpected_test_results.append(result)
        else:
            if (self.format != "stats"
                and result.GetOutcome() != result.PASS):
                self.__unexpected_resource_results.append(result)
            
        # In some modes, no results are displayed as the tests are run.
        if self.format == "batch" or self.format == "stats":
            return
        
        # Display a heading before the first result.
        if self.__first_test:
            self._DisplayHeading("TEST RESULTS")
            self.__first_test = 0
        
        # Display the result.
        self._DisplayResult(result, self.format)

        # Display annotations associated with the test.
        if (self.format == "full"
            or (self.format == "brief"
                and result.GetOutcome() != Result.UNTESTED # See CORALCOOL-1526 aka task #18784
                and result.GetOutcome() != Result.PASS)):
            self._DisplayAnnotations(result)


    def Summarize(self):
        """Output summary information about the results.

        When this method is called, the test run is complete.  Summary
        information should be displayed for the user, if appropriate.
        Any finalization, such as the closing of open files, should
        also be performed at this point."""

        if self.format == "batch":
            self._DisplayStatistics()

        # Show results for tests with unexpected outcomes.
        if self.format in ("full", "brief", "batch"):
            compare_ids = lambda r1, r2: cmp(r1.GetId(), r2.GetId())

            # Sort test results by ID.
            self.__unexpected_test_results.sort(compare_ids)
            # Print individual test results.
            if self.expected_outcomes:
                self._DisplayHeading("TESTS WITH UNEXPECTED OUTCOMES")
            else:
                self._DisplayHeading("TESTS THAT DID NOT PASS (EXCLUDING UNTESTED TESTS)") # See CORALCOOL-1549 aka task #21088
            self._SummarizeResults(self.__unexpected_test_results)

            if self.__unexpected_resource_results:
                # Sort resource results by ID.
                self.__unexpected_resource_results.sort(compare_ids)
                # Print individual resource results.
                self._DisplayHeading("RESOURCES THAT DID NOT PASS")
                self._SummarizeResults(self.__unexpected_resource_results)

        if self.format != "batch":
            self._DisplayStatistics()
        
        # Invoke the base class method.
        super(TextResultStream, self).Summarize()


    def _SummarizeTestStats(self):
        """Generate statistics about the overall results."""

        # Display AA nightlies info
        # See CORALCOOL-1083 aka bug #86463
        # See CORALCOOL-1551 aka task #21216

        self.file.write("\n")
        self._DisplayHeading("ENVIRONMENT")
        for env in "HOST", "CMTCONFIG", "BINARY_TAG", "SLOTNAME", "COOL_PYCOOLTEST_SKIP_EXCEPTIONS", "COOL_PYCOOLTEST_SKIP_ROOT6927", "COOL_PYCOOLTEST_SKIP_ROOT8458", "CORAL_SQLITE_SYNCHRONOUS_OFF" :
            self.file.write( "%s = %s\n" % ( env, os.environ[env] if env in os.environ else "[not set]" ) )
        for env in "CORAL_AUTH_PATH", "CORAL_DBLOOKUP_PATH", "TNS_ADMIN", "COOLSYS", "CORALSYS", "ROOTSYS" :
            self.file.write( "%s = %s\n" % ( env, os.path.abspath(os.environ[env]) if env in os.environ else "[not set]" ) )
        import socket
        self.file.write( "%s = %s\n" % ( "hostname", socket.gethostname() ) )
        # Display Kerberos tickets and AFS tokens (debug bug #102099)
        import subprocess
        from subprocess import CalledProcessError
        self.file.write("\n")
        try:
            from subprocess import check_output, STDOUT
            self._DisplayHeading("ENVIRONMENT: KERBEROS TICKETS")
            try: self.file.write( check_output(["klist"],stderr=STDOUT) )
            except CalledProcessError,e: self.file.write( e.output )
            except OSError,e: self.file.write( "klist failed (" + e.strerror + ")\n" )
            import platform
            if platform.system() != "Darwin" : # Skip tokens on mac (hangs on macitois18, SPI-925)
                self.file.write("\n")
                self._DisplayHeading("ENVIRONMENT: AFS TOKENS")
                try: self.file.write( check_output(["tokens"],stderr=STDOUT) )
                except CalledProcessError,e: self.file.write( e.output ) 
                except OSError,e: self.file.write( "tokens failed (" + e.strerror + ")\n" )
        except ImportError, e:
            pass # subprocess.check_output is missing in python 24

        # Print a header.
        self.file.write("\n")
        self._DisplayHeading("STATISTICS")

        # Build the format string.  If there are no tests we do not
        # provide any output.
        if self.__num_tests != 0:
            # Indicate the total number of tests.
            format = "  %(TOTAL)6d        tests total\n"
            # Include a line for each outcome.
            for o in Result.outcomes:
                if self.__outcome_counts[o] != 0:
                    format += ("  %%(%s)6d (%%(%s)3.0f%%%%) tests %s\n"
                               % (o, o + "_PERCENT", o))
            format += "\n"
        else:
            format = ""

        self._FormatStatistics(format)


    def _SummarizeResults(self, results):
        """Summarize each of the results.

        'results' -- The sequence of 'Result' objects to summarize."""

        if len(results) == 0:
            self.file.write("  None.\n\n")
            return

        # Generate them.
        for result in results:
            if result.GetOutcome() != Result.UNTESTED : # See CORALCOOL-1549 aka task #21088
                self._DisplayResult(result, self.format)
            if self.format == "batch" and result.GetOutcome() != Result.UNTESTED : # See CORALCOOL-1526 aka task #18784
                self._DisplayAnnotations(result)
