To run only a subset of tests, for instance for frontier:
  tests=`find . -name '*frontier*' | grep -v .svn | sed "s|\./||g" | sed "s|\.qms/|.|g" | sed "s|\.qmt||g"`
Then run the qmtest wrapper taking "$tests" as argument  
