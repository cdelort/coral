#ifndef MYSQLACCESS_TESTBASE_H
#define MYSQLACCESS_TESTBASE_H

#include <iostream>
#include <string>
#include "CoralBase/../tests/Common/CoralCppUnitDBTest.h"
#include "RelationalAccess/AccessMode.h"
#include "RelationalAccess/ConnectionService.h"
#include "RelationalAccess/ISessionProxy.h"

// The connection strings for CORAL tests
const std::string coralConnStringAdmin = coral::CoralCppUnitDBTest::BuildUrl( "MySQL", false );

//-----------------------------------------------------------------------------

class TestBase
{

public:

  // Constructor
  TestBase(){}

  // Destructor
  virtual ~TestBase(){}

  // The actual run method
  virtual void run() = 0;

protected:

  // Returns a session object
  coral::ISessionProxy* connect( const std::string& connectionString = coralConnStringAdmin )
  {
    coral::ConnectionService connSvc;
    return connSvc.connect( connectionString );
  }
  
};

//-----------------------------------------------------------------------------

#endif
