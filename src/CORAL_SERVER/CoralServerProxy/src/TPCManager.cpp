//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id: TPCManager.cpp,v 1.2.2.5.2.1 2012-11-21 10:59:02 avalassi Exp $
//
// Description:
//	Class TPCManager...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "TPCManager.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <iostream>
#include <cstring>
#include <signal.h>
#include "CoralBase/../src/coral_thread_headers.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ClientConnManager.h"
#include "ClientReaderFactory.h"
#include "ClientWriter.h"
#include "MsgLogger.h"
#include "PacketDispatcher.h"
#include "PacketHeaderQueue.h"
#include "PacketQueue.h"
#include "RoutingTables.h"
#include "ServerReaderFactory.h"
#include "ServerWriter.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

// File descriptor use for accepting connections
coral::CoralServerProxy::NetSocket _accept_socket;

// signal handler for terminating events
void
sig_handler_term ( int sigval )
{
  if (sigval == SIGINT || sigval == SIGTERM) {
    // close socket, this will stop the world, it is
    // async-signal-safe, only calls close(2)
    _accept_socket.close();
  }
}

}

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

namespace coral {
namespace CoralServerProxy {

//----------------
// Constructors --
//----------------
TPCManager::TPCManager ( const NetEndpointAddress& listenAddress,
                         bool overridePortmap,
                         const std::vector<NetEndpointAddress>& serverAddress,
                         size_t maxQueueSize,
                         unsigned timeout,
                         IPacketCache& cache,
                         StatCollector* statCollector,
                         bool keepUpstreamOpen,
                         unsigned maxAccepErrors,
                         bool unregister_portmap,
                         const std::string& pmap_lock_dir)
  : m_listenAddress( listenAddress )
  , m_overridePortmap( overridePortmap )
  , m_serverAddress( serverAddress )
  , m_maxQueueSize( maxQueueSize )
  , m_timeout( timeout )
  , m_cache( cache )
  , m_statCollector( statCollector )
  , m_keepUpstreamOpen(keepUpstreamOpen)
  , m_maxAccepErrors(maxAccepErrors)
  , m_unregisterPortmap(unregister_portmap)
  , m_pmapLockDir(pmap_lock_dir)
{
  PXY_TRACE ( "TPCManager: listenAddress = " << listenAddress ) ;
  for (std::vector<NetEndpointAddress>::const_iterator it = serverAddress.begin(); it != serverAddress.end(); ++ it) {
    PXY_TRACE ( "TPCManager: serverAddress = " << *it ) ;
  }
}

//--------------
// Destructor --
//--------------
TPCManager::~TPCManager ()
{
}

// run method runs until signal is received.
int
TPCManager::run()
{
  PXY_DEBUG( "TPCManager: starting TPCManager" );

  // we will listen on a specific port and create a new thread
  // for every incoming connection

  // create listening socket
  NetSocket sock = setupListener () ;
  if ( not sock.isOpen() ) return -1;
  unsigned short port = sock.local().port().port();

  // setup all three queues for packets
  PacketQueue rcvQueue ( m_maxQueueSize ) ;
  PacketQueue serverQueue ( m_maxQueueSize ) ;
  PacketHeaderQueue clientQueue ( m_maxQueueSize ) ;

  // instantiate routing tables
  RoutingTables routing ;

  // client connection manager
  ServerReaderFactory serverReaderFactory ( m_serverAddress, rcvQueue, m_timeout ) ;
  ClientReaderFactory clientReaderFactory ( rcvQueue, routing, m_timeout ) ;
  ClientConnManager connMgr ( serverReaderFactory, clientReaderFactory, rcvQueue, serverQueue, clientQueue, routing, m_keepUpstreamOpen ) ;

  // start packet dispatcher thread
  PacketDispatcher dispatcher ( rcvQueue, serverQueue, clientQueue, m_cache, m_statCollector ) ;
  coral::thread dispatcherThread( dispatcher ) ;

  // start client writing thread
  coral::thread clientWriterThread( ClientWriter ( clientQueue, routing, connMgr ) ) ;

  // start server writer thread
  coral::thread serverWriterThread( ServerWriter ( serverQueue, connMgr ) ) ;

  PXY_INFO("Setting up signal handlers");
  signal(SIGINT, ::sig_handler_term);
  signal(SIGTERM, ::sig_handler_term);
  ::_accept_socket = sock;

  unsigned failures = 0;
  while ( true ) {

    // accept new connections
    NetSocket cli_sock ( sock.accept() ) ;
    if ( not cli_sock.isOpen() ) {
      if (errno == EBADF) {
        // means that socket was closed
        PXY_INFO("TPCManager: signal received (accept socket closed), stopping");
        break;
      }
      PXY_ERR("TPCManager: accept failed: " << strerror(errno));
      if (errno != EINTR && m_maxAccepErrors > 0 && ++failures > m_maxAccepErrors) {
        // Too many failures in a row, time to die
        PXY_ERR("TPCManager: Exceeded failure limit (" << m_maxAccepErrors << "), stopping");
        break;
      }
    } else {
      PXY_INFO ( "TPCManager: new client connection " << cli_sock
          << " from " << cli_sock.peer() ) ;
      failures = 0;

      // add this connection to the connection manager
      // this should happen before we start checking the server connection
      if ( not connMgr.addConnection ( cli_sock ) ) {

        // could not get server connection up, server is probably gone for good.
        // We want to close an incoming connection.
        // We could also send client a special error packet with some
        // specific message or code.

        cli_sock.close() ;

      }
    }
  }

  PXY_DEBUG( "TPCManager: cleaning up TPCManager" );

  // unregister from portmap
  if (m_unregisterPortmap) {
    m_listenAddress.unregisterPort(port);
  }

  // no way now to stop threads properly but we have to detach them
  // otherwise we will have nasty terminate() call
  dispatcherThread.detach();
  clientWriterThread.detach();
  serverWriterThread.detach();

  return 0;
}

// start listening
NetSocket
TPCManager::setupListener ()
{
  // create socket
  PXY_DEBUG ( "TPCManager: Creating socket" );
  NetSocket sock ( PF_INET, SOCK_STREAM, 0 ) ;
  if ( not sock.isOpen() ) {
    PXY_ERR ( "TPCManager: Failed to create a socket: " << strerror(errno) );
    return sock ;
  }

  // set socket options
  if ( setSocketOptions( sock ) < 0 ) {
    PXY_ERR ( "TPCManager: Failed to set socket options " << sock << ": " << strerror(errno) );
    return NetSocket() ;
  }

  // bind the socket
  PXY_DEBUG( "Binding socket " << sock );
  if ( sock.bind ( m_listenAddress ) < 0 ) {
    PXY_ERR( "TPCManager: Failed to bind the socket: " << strerror(errno) );
    return NetSocket() ;
  } else {
      PXY_DEBUG( "Bound socket to port " << sock.local().port().port() );
  }

  // register port with portmapper if needed
  unsigned short port = sock.local().port().port();
  if (!m_listenAddress.registerPort(port, m_overridePortmap, m_pmapLockDir)) {
    PXY_ERR("Failed to registered port with portmapper " << port << " -> " << m_listenAddress.port());
    return NetSocket() ;
  }

  // listen on this socket
  PXY_DEBUG( "Listening socket " << sock << ", maxconn = " << SOMAXCONN );
  if ( sock.listen ( SOMAXCONN ) < 0 ) {
    PXY_ERR( "TPCManager: Failed to listen the socket: " << strerror(errno) );
    return NetSocket() ;
  }

  return sock ;
}

int
TPCManager::setSocketOptions( NetSocket& sock )
{
  // reuse the address
  if ( sock.setSocketOptions ( SOL_SOCKET, SO_REUSEADDR, 1 ) < 0 ) {
    return -1 ;
  }
  if ( sock.setSocketOptions ( SOL_SOCKET, SO_KEEPALIVE, 1 ) < 0 ) {
    return -1 ;
  }
  if ( sock.setSocketOptions ( IPPROTO_TCP, TCP_NODELAY, 1 ) < 0 ) {
    return -1 ;
  }
  return 0 ;
}

} // namespace CoralServerProxy
} // namespace coral
