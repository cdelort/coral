#ifndef CORALSOCKETS_ISOCKET_H
#define CORALSOCKETS_ISOCKET_H 1

// Include files
#include <memory>
#include "CoralServerBase/CoralServerBaseException.h"

namespace coral {

  // forward declaration
  class ICertificateData;

  namespace CoralSockets {

    // forward declaration
    class ISocket;
    class SocketContext;

    typedef std::shared_ptr<ISocket> ISocketPtr;

    class ISocket 
    {

    public:

      enum PMode {
        P_READ,
        P_WRITE,
        P_READ_WRITE
      };

      /// Fix Coverity UNINIT_CTOR
      /// NB This is NOT an abstract interface...
      ISocket() : m_context(0), m_sContext() 
      {
      }

      /// reads len bytes from the socket into *buf
      /// doesn't return until all bytes are read, or some error occurs
      virtual void readAll( unsigned char *buf, size_t len) = 0;

      /// reads at most len bytes from the socket into *buf
      /// returns the number of bytes actually read
      /// returns early if it could not read anything for timeout miliseconds
      /// timeout equal to <0 means try for ever
      virtual size_t read( unsigned char *buf, size_t len,
                           int timeout = 0) = 0;

      /// writes len bytes into the socket into *buf
      /// doesn't return until all bytes are read, or some error occurs
      virtual void writeAll( const unsigned char *buf, size_t len) = 0;

      /// writes len bytes from *buf to the socket
      /// returns the number of bytes actually written
      /// returns early if it could not write anyting for timeout miliseconds
      /// timeout equal to <0 means try for ever
      virtual size_t write( const unsigned char *buf, size_t len,
                            int timeout = 0 ) = 0;

      /// polls if the socket is available for
      /// reading or writing depending on mode
      /// times out after timeout milliseconds
      /// returns true if the socket is available
      virtual bool poll( PMode mode, int timeout ) = 0;

      /// closes the socket
      virtual void close() = 0;

      /// returns true if the socket is open
      virtual bool isOpen() const = 0;

      /// returns a string describing the socket
      virtual const std::string& desc() const = 0;

      /// cork the socket
      /// tells the socket only full packets should be put on the line.
      virtual void cork() {};

      /// uncork the socket
      /// send all remaining data in the buffer out and return to normal
      /// operation mode
      virtual void uncork() {};

      /// returns the associated certificate data if available.
      /// If no data is availabe it returns 0
      virtual const coral::ICertificateData* getCertificateData() const
      { return (coral::ICertificateData*) 0; };

      virtual int getFd() const = 0;

      virtual ~ISocket() {};

      // for stressClient
      void setContext( int context)
      {
        m_context = context;
      }

      // for stressClient
      int getContext() const
      {
        return m_context;
      }

      void setSContext( std::shared_ptr<SocketContext> context)
      {
        m_sContext = context;
      }

      std::shared_ptr<SocketContext> getSContext() const
      {
        return m_sContext;
      }

      virtual std::string remoteEnd() const
      {
        return std::string("unknown");
      }

      virtual std::string localEnd() const
      {
        return std::string("unknown");
      }

    private:

      int m_context;
      std::shared_ptr<SocketContext> m_sContext;

    };

    class SocketClosedException : public CoralServerBaseException
    {

    public:

      /// Constructor
      SocketClosedException( const std::string& methodName = "" )
        : CoralServerBaseException( "Socket is closed", methodName, "coral::CoralSockets" ) {}

      /// Destructor
      virtual ~SocketClosedException() throw() {}

    };


  }
}


#endif // CORALSOCKETS_TCPSOCKET_H
