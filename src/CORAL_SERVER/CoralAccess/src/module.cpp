
// Include files
#include "CoralKernel/CoralPluginDef.h"

// Local include files
#include "Domain.h"

//-----------------------------------------------------------------------------

CORAL_PLUGIN_MODULE( "CORAL/RelationalPlugins/coral",
                     coral::CoralAccess::Domain )

//-----------------------------------------------------------------------------
