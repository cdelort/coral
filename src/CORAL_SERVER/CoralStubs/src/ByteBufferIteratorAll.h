#ifndef CORAL_CORALSTUBS_BYTEBUFFERITERATORALL_H
#define CORAL_CORALSTUBS_BYTEBUFFERITERATORALL_H

//CoralServer includes
#include "CoralServerBase/CALOpcode.h"
#include "CoralServerBase/IByteBufferIterator.h"
#include "CoralServerBase/IRowIterator.h"

//Coral includes
#include "CoralBase/AttributeList.h"

//CoralServerStubs includes
#include "SegmentWriterIterator.h"

namespace coral {

  // Forward declaration
  struct RequestProperties;
  struct ConnectionProperties;

  namespace CoralStubs {

    class ByteBufferIteratorAll : public IByteBufferIterator {
    public:

      /**
       * @param rowi: result row iterator
       * @param cacheable: cacheable flag to set in packet header
       * @param proxy: proxy flag to set in packet header
       * @param isempty: if true then query does not define row buffer and
       *                 rowBuffer argument is not used
       * @param rowBuffer: row buffer used for queries if isempty is false
       * @param schema: schema name (for monitoring purposes only)
       * @param tables: list of table names (for monitoring purposes only)
       * @param cprop: connection properties (for monitoring purposes only)
       * @param properties: request properties (for monitoring purposes only)
       */
      ByteBufferIteratorAll(IRowIterator* rowi,
                            CALOpcode opcode,
                            bool cacheable,
                            bool proxy,
                            bool isempty,
                            AttributeList* rowBuffer,
                            const std::string& schema,
                            const std::list< std::string >& tables,
                            const ConnectionProperties* cprop,
                            const RequestProperties& properties);

      ~ByteBufferIteratorAll();

      bool nextBuffer();

      bool isLastBuffer() const;

      const ByteBuffer& currentBuffer() const;

    private:

      /// Copy constructor is private (fix Coverity MISSING_COPY)
      ByteBufferIteratorAll( const ByteBufferIteratorAll& rhs );

      /// Assignment operator is private (fix Coverity MISSING_ASSIGN)
      ByteBufferIteratorAll& operator=( const ByteBufferIteratorAll& rhs );

      void fillBuffer();

      SegmentWriterIterator m_swi;

      IRowIterator* m_rowi;

      AttributeList* m_rowBuffer;

      bool m_isempty;

      void* m_structure;

      bool m_islast;     // set to true after last row is read from row iterator

      bool m_lastbuffer;

      // For Monitoring purpose
      std::string m_schema;

      std::list< std::string > m_tables;

      std::string m_address;

      uint32_t m_connid;

      int m_requestid;

      double m_montime;

      size_t m_monsize;

      size_t m_nextBufferCount; // how many times nextBuffer() returned true

    };

  }

}

#endif
