#ifndef CORAL_CORALSTUBS_EXCEPTIONS_H
#define CORAL_CORALSTUBS_EXCEPTIONS_H 1

// Include files
#include "CoralServerBase/CoralServerBaseException.h"

namespace coral {

  namespace CoralStubs {

    // Base exception class for CoralStubs
    class StubsException : public CoralServerBaseException
    {
    public:
      // Constructor
      StubsException( const std::string& message ) : CoralServerBaseException( message, "StubsException", "coral::CoralStubs" ){}
      // Destructor
      virtual ~StubsException() throw() {}
    };

    /*
    class DBProxyException : public CoralServerBaseException
    {
    public:
      // Constructor
      DBProxyException( const std::string& message ) : CoralServerBaseException( "Exception from the DBProxy ( " + message + " )", "DBProxyException", "coral::CoralStubs" ){}
      // Destructor
      virtual ~DBProxyException() throw() {}
    };
    */

    class InvalidMessageException : public CoralServerBaseException
    {
    public:
      // Constructor
      InvalidMessageException( const std::string& message, const std::string& location ) : CoralServerBaseException( message, location, "coral::CoralStubs" ){}
      // Destructor
      virtual ~InvalidMessageException() throw() {}
    };

    class ReplyIteratorException : public CoralServerBaseException
    {
    public:
      // Constructor
      ReplyIteratorException( const std::string& message, const std::string& location ) : CoralServerBaseException( message, location, "coral::CoralStubs" ){}
      // Destructor
      virtual ~ReplyIteratorException() throw() {}
    };

    class StreamBufferException : public CoralServerBaseException
    {
    public:
      // Constructor
      StreamBufferException( const std::string& message, const std::string& location ) : CoralServerBaseException( message, location, "coral::CoralStubs" ){}
      // Destructor
      virtual ~StreamBufferException() throw() {}
    };

  }

}

#endif
